package com.audify.app;

import android.content.Context;
import android.database.Cursor;
import android.database.MergeCursor;
import android.provider.MediaStore;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.audify.app.model.Songs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import kotlinx.coroutines.GlobalScope;

public class MainViewModel extends ViewModel {
    private MutableLiveData<ArrayList<Songs>> mutableLiveData;



    public MainViewModel() {
        mutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<ArrayList<Songs>> getAllMediaFilesOnDevice(Context context) {
        ArrayList<Songs> songList = new ArrayList<>();
        List<com.audify.app.dp.Songs> favoriteSongList = Application.getInstance().getAppDatabase().newsDao().getFavoriteSongs();
        try {
            final String[] columns = { MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media.DATE_ADDED,
                    MediaStore.Images.Media.BUCKET_ID,
                    MediaStore.Images.Media.BUCKET_DISPLAY_NAME };

            MergeCursor cursor = new MergeCursor(new Cursor[]{context.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, columns, null, null, null),});
            cursor.moveToFirst();
            songList.clear();
            while (!cursor.isAfterLast()){
                String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
                int lastPoint = path.lastIndexOf(".");
                path = path.substring(0, lastPoint) + path.substring(lastPoint).toLowerCase();
                File file = new File(path);
                Songs songs = new Songs();
                songs.setSongName(file.getName().substring(0, file.getName().length() - 3));
                songs.setSongAlbumName("Alubum : "+file.getName());
                for(com.audify.app.dp.Songs favoriteSong :favoriteSongList) {
                    if(favoriteSong.getName().equals(songs.getSongName())) {
                        songs.setFavourite(true);
                    }
                }
                songList.add(songs);
                cursor.moveToNext();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mutableLiveData.setValue(songList);
        return mutableLiveData;
    }

    public void setFavoriteSong(String songName) {
        com.audify.app.dp.Songs songs = new com.audify.app.dp.Songs();
        songs.setName(songName);
        Application.getInstance().getAppDatabase().newsDao().insert(songs);
    }

    public void setDeFavoriteSong(String songName) {
        Application.getInstance().getAppDatabase().newsDao().deleteSong(songName);
    }
}
