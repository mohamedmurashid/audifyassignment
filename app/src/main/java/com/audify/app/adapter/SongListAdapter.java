package com.audify.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.audify.app.R;
import com.audify.app.model.Songs;

import java.util.ArrayList;

public class SongListAdapter extends RecyclerView.Adapter<SongListAdapter.ViewHolder> {

    ArrayList<Songs> files;
    OnFavoriteClickListener onFavoriteClickListener;
    public SongListAdapter(ArrayList<Songs> files, OnFavoriteClickListener onFavoriteClickListener) {
        this.files = files;
        this.onFavoriteClickListener = onFavoriteClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.songs_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvSong.setText(files.get(position).getSongName());
        holder.tvAlbumName.setText(files.get(position).getSongAlbumName());
        holder.ivFavourite.setImageResource(files.get(position).isFavourite() ? R.drawable.ic_favorite_selected :
                R.drawable.ic_favorite_border);
    }

    @Override
    public int getItemCount() {
        return files.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvSong, tvAlbumName;
        ImageView ivFavourite;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvSong = itemView.findViewById(R.id.tvSong);
            tvAlbumName = itemView.findViewById(R.id.tvAlbumName);
            ivFavourite = itemView.findViewById(R.id.ivFavourite);

            ivFavourite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    files.get(position).setFavourite(!files.get(position).isFavourite());
                    ivFavourite.setImageResource(files.get(position).isFavourite() ? R.drawable.ic_favorite_selected :
                            R.drawable.ic_favorite_border);
                    if (files.get(position).isFavourite() ) {
                        onFavoriteClickListener.onFavorite(files.get(position).getSongName());
                    } else {
                        onFavoriteClickListener.onDeFavorite(files.get(position).getSongName());
                    }
                }
            });
        }
    }

    public interface OnFavoriteClickListener
    {
        void onFavorite(String songName);
        void onDeFavorite(String songName);
    }
}
