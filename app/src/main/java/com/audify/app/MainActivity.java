package com.audify.app;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

import com.audify.app.adapter.SongListAdapter;
import com.audify.app.model.Songs;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SongListAdapter.OnFavoriteClickListener {

    private MainViewModel mViewModel;
    private ArrayList<Songs> files;
    private  SongListAdapter songListAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        files = new ArrayList<>();
        RecyclerView recyclerView = findViewById(R.id.rvSongList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        songListAdapter = new SongListAdapter(files, this);
        recyclerView.setAdapter(songListAdapter);
        mViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        requestPermissions();
    }

    private void requestPermissions() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        200);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            getSongs();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 200 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getSongs();
        }
    }

    private void getSongs() {
        mViewModel.getAllMediaFilesOnDevice(this).observe(this, new Observer<ArrayList<Songs>>() {
            @Override
            public void onChanged(ArrayList<Songs> songs) {
                if(songs != null) {
                    files.addAll(songs);
                    songListAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public void onFavorite(String songName) {
        mViewModel.setFavoriteSong(songName);
    }

    @Override
    public void onDeFavorite(String songName) {
        mViewModel.setDeFavoriteSong(songName);
    }
}