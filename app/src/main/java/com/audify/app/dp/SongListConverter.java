package com.audify.app.dp;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

public class SongListConverter implements Serializable {

    @TypeConverter
    public String fromNewsList(List<Songs> news) {
        if (news == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Songs>>() {
        }.getType();
        return gson.toJson(news, type);
    }

    @TypeConverter
    public List<Songs> toNewsList(String optionValuesString) {
        if (optionValuesString == null) {
            return (null);
        }
        Gson gson = new Gson();
        Type type = new TypeToken<List<Songs>>() {
        }.getType();
        return gson.fromJson(optionValuesString, type);
    }

}