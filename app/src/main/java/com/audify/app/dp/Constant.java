package com.audify.app.dp;

public interface Constant {
    String DATABASE_NAME = "AudifyDB";
    String SONGS_TABLE_NAME = "Songs";
    String SONGS_NAME = "SongsName";
}
