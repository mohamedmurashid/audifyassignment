package com.audify.app.dp;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;


import java.io.Serializable;

@Entity(tableName = Constant.SONGS_TABLE_NAME)
@TypeConverters(SongListConverter.class)
public class Songs implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int news_id;

    @ColumnInfo(name = Constant.SONGS_NAME)
    private String name;

    public int getNews_id() {
        return news_id;
    }

    public void setNews_id(int news_id) {
        this.news_id = news_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
