package com.audify.app.dp;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.audify.app.Application;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Songs.class}, version = 1)
@TypeConverters(SongListConverter.class)
public abstract class AppDatabase extends RoomDatabase {

    public abstract SongsDAO newsDao();

    private static volatile AppDatabase INSTANCE;

    public static AppDatabase getDatabase(final Application context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context,
                            AppDatabase.class, Constant.DATABASE_NAME)
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}
