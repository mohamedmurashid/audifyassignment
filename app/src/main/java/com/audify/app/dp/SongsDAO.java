package com.audify.app.dp;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;


@Dao
public interface SongsDAO {

    @Query("SELECT * FROM "+ Constant.SONGS_TABLE_NAME)
    List<Songs> getFavoriteSongs();

    @Insert
    void insert(Songs song);

    @Query("DELETE FROM "+ Constant.SONGS_TABLE_NAME +" WHERE "+ Constant.SONGS_NAME +"=:song")
    void deleteSong(String song);
}
