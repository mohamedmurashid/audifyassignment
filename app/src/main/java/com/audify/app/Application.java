package com.audify.app;


import com.audify.app.dp.AppDatabase;

public class Application extends android.app.Application {

    private static Application mInstance;
    private AppDatabase mAppDatabase;

    public static synchronized Application getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mAppDatabase = AppDatabase.getDatabase(this);
    }

    public AppDatabase getAppDatabase() {
        return mAppDatabase;
    }


}
